import { shade } from 'polished';
import styled from 'styled-components';

export const Container = styled.button`
    margin-top: 10px;
    cursor: pointer;
    width: 300px;
    height: 42px;
    border-radius: 4px;
    border: 0;
    background: #57bbbc;

    color: #fff;
    font-weight: bold;
    font-size: 16px;
    transition: background 0.2s;

    &:hover {
        background: ${shade(0.2, '#57bbbc')};
    }
`;

import React, {
  ChangeEvent, useCallback, useEffect, useState,
} from 'react';

import { FiSearch } from 'react-icons/fi';
import { Link } from 'react-router-dom';

import debounce from 'lodash/debounce';

import { useDispatch, useSelector } from 'react-redux';
import headerLogo from '../../assets/logo-header.png';
import Input from '../Input';

import { Container, MainHeader } from './styles';
import { getEnterprisesRequest, getEnterprisesReset } from '../../store/modules/enterprises/actions';
import { StoreState } from '../../store/createStore';

interface HeaderProps {
  isSearch?: boolean;
}

const Header = ({ isSearch }: HeaderProps): JSX.Element => {
  const [enterprise, setEnterprise] = useState<string>('');

  const { access_token, client, uid } = useSelector((state: StoreState) => state.auth);

  const dispatch = useDispatch();

  const handleResetEnterprisesRequest = useCallback(() => {
    dispatch(getEnterprisesReset());
    setEnterprise(() => '');
  }, [dispatch]);

  const getEnterprises = useCallback((value: string) => {
    if (access_token && client && uid) {
      dispatch(getEnterprisesRequest({
        access_token, client, uid, enterprise_name: value,
      }));
    }
  }, [dispatch, access_token, client, uid]);

  const handleSearchEnterprise = useCallback(
    debounce((value) => getEnterprises(value), 500),
    [],
  );

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setEnterprise(event.target.value);
    handleSearchEnterprise(event.target.value);
  };

  return (
    <Container>

      {isSearch ? (
        <div>
          <Input
            icon={FiSearch}
            isClosed
            placeholder="Pesquisar"
            value={enterprise}
            onChange={onChange}
            handleReset={handleResetEnterprisesRequest}
          />
        </div>
      ) : (
        <MainHeader>
          <div>
            <div />
            <img src={headerLogo} alt="header-logo" />

            <Link to="/search">
              <FiSearch color="#fff" size={22} />

            </Link>
          </div>
        </MainHeader>
      )}

    </Container>
  );
};

export default Header;

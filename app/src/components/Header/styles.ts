import styled from 'styled-components';

export const Container = styled.header`
  background: linear-gradient(173deg, #ee4c77 24%,  #ee4c77  66%);
  height: 93px;
  width: 100%;
  padding: 0 32px;
  display: flex;
  align-items: center;

  > div {
    width: 100%;
  }
`;

export const MainHeader = styled.div`
  width: 100%;

  > div {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

import styled, { css } from 'styled-components';

interface IContainerProps {
    isFocused?: boolean;
    isFilled?: boolean;
    isClosed?: boolean;
}

export const Wrapper = styled.div``;

export const Container = styled.div<IContainerProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    border-bottom: 0.5px solid #383743;
    padding: 10px 15px;
    width: 100%;

    height: 42px;
    margin-bottom: 10px;

    svg {
        margin-right: 5px;
        color: #ee4c77;
    }

    ${(props) => props.isFocused
        && css`
            svg {
                color: #ee4c77;
            }
            border-color: #ee4c77;
        `}

    ${(props) => props.isFilled
        && css`
            svg {
                color: #ee4c77;
            }
        `}

    ${(props) => props.isClosed
    && css`

      border-bottom: 0.5px solid #fff;
        svg {
            color: #fff;
        }
    `}

    input {
        flex: 1;
        height: 100%;
        padding-left: 8px;
        font-size: 1.6rem;
        background: transparent;
        color: #403e4d;
        border: 0;

        &::placeholder {
            color: #383743;
        }

        ${(props) => props.isClosed && css`
          color: #fff;

            &::placeholder {
              color: #991237;
              font-size: 1.8rem;
            }
        `}
    }

    .icon-click {
        cursor: pointer;
    }
`;

export const ErrorText = styled.span`
  color: #ff4040;
  margin: 2px 0;
  font-size: 13px;
`;

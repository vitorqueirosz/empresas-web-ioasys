import React, {
  ComponentType,
  InputHTMLAttributes,
  useState,
  useCallback,
} from 'react';

import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { IoMdAlert } from 'react-icons/io';
import { IconBaseProps } from 'react-icons';

import { FiX } from 'react-icons/fi';
import { Wrapper, Container, ErrorText } from './styles';

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  icon: ComponentType<IconBaseProps>;
  isPassword?: boolean;
  error?: string;
  isError?: boolean;
  focused?: boolean;
  isFilled?: boolean;
  submitError?: boolean;
  isClosed?: boolean;
  handleReset?: () => void;
}

const Input: React.FC<IProps> = ({
  icon: Icon,
  isPassword = false,
  error,
  focused,
  isError,
  isFilled,
  submitError,
  isClosed,
  handleReset,
  ...rest
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const [isFocused, setIsFocused] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(() => true);
  }, []);

  return (
    <Wrapper>
      <Container isFilled={isFilled} isFocused={isFocused && focused} isClosed={isClosed}>
        {Icon && <Icon size={16} />}
        <input
          {...rest}
          type={showPassword ? 'text' : rest.type}
          onFocus={handleInputFocus}
        />
        {isPassword && !submitError && isFocused
              && (showPassword ? (
                <FaEyeSlash
                  size={16}
                  color="rgba(0, 0, 0, 0.54)"
                  onClick={() => setShowPassword(!showPassword)}
                  className="icon-click"
                />
              ) : (
                <FaEye
                  size={16}
                  color="rgba(0, 0, 0, 0.54)"
                  onClick={() => setShowPassword(!showPassword)}
                  className="icon-click"
                />
              ))}

        {submitError && (
        <IoMdAlert
          size={16}
          color="#ff0f44"
        />
        )}

        {isClosed && isFocused && (
          <FiX
            size={16}
            color="#fff"
            className="icon-click"
            onClick={handleReset}
          />
        )}

      </Container>
      {isError && <ErrorText>{error}</ErrorText>}
    </Wrapper>
  );
};

export default Input;

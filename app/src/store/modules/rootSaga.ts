import { all } from 'redux-saga/effects';
import auth from './auth/sagas';
import enterprises from './enterprises/sagas';
import enterprise from './enterprise/sagas';

export default function* root() {
  yield all([
    auth,
    enterprises,
    enterprise,
  ]);
}

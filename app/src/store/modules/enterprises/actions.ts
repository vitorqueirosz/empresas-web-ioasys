import { action } from 'typesafe-actions';
import { Enterprise } from './types';

export function getEnterprisesRequest({
  access_token,
  client,
  uid,
  enterprise_name,
}: {
  access_token: string;
  client: string;
  uid: string;
  enterprise_name: string;
}) {
  return action('@enterprise/GET_ENTERPRISES_REQUEST', {
    access_token,
    client,
    uid,
    enterprise_name,
  });
}

export function getEnterprisesSuccess({ enterprises }: {
  enterprises: Enterprise[]}) {
  return action('@enterprise/GET_ENTERPRISES_SUCCESS', {
    enterprises,
  });
}

export function getEnterprisesReset() {
  return action('@enterprise/GET_ENTERPRISES_RESET');
}

export function getEnterprisesFailure({ message }: { message: string }) {
  return action('@enterprise/GET_ENTERPRISES_FAILURE', { message });
}

import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

export function* fetchEnterprises({ payload }: ActionType<typeof actions.getEnterprisesRequest>) {
  try {
    const { client, uid, enterprise_name } = payload;

    const { data } = yield call(api.get, 'enterprises', {
      params: { name: enterprise_name },
      headers: {
        'access-token': payload.access_token,
        client,
        uid,
      },
    });

    console.log(data);

    yield put(actions.getEnterprisesSuccess({ enterprises: data.enterprises }));
  } catch (err) {
    yield put(actions.getEnterprisesFailure({ message: '' }));
  }
}

export default all([takeLatest('@enterprise/GET_ENTERPRISES_REQUEST', fetchEnterprises)]);

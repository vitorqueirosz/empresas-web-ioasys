import { EnterprisesAction, EnterprisesState } from './types';

const INTIAL_STATE: EnterprisesState = {
  loadingEnterprisesRequest: false,
  loadingEnterprisesSuccess: false,
  error: false,
  access_token: null,
  client: null,
  uid: null,
  errMessage: null,
  enterprise_name: null,
  enterprises: [],
};

export default function auth(
  state = INTIAL_STATE,
  action: EnterprisesAction,
): EnterprisesState {
  switch (action.type) {
    case '@enterprise/GET_ENTERPRISES_REQUEST':
      return {
        ...state,
        loadingEnterprisesRequest: true,
        loadingEnterprisesSuccess: false,
      };

    case '@enterprise/GET_ENTERPRISES_SUCCESS':
      return {
        ...state,
        loadingEnterprisesRequest: false,
        enterprises: action.payload.enterprises,
        loadingEnterprisesSuccess: true,
      };

    case '@enterprise/GET_ENTERPRISES_RESET':
      return {
        ...state,
        loadingEnterprisesRequest: false,
        enterprises: [],
        errMessage: null,
      };

    case '@enterprise/GET_ENTERPRISES_FAILURE':
      return {
        ...state,
        loadingEnterprisesRequest: false,
        error: true,
        errMessage: action.payload.message,
      };

    default:
      return state;
  }
}

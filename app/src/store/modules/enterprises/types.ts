import { ActionType } from 'typesafe-actions';

import * as actions from './actions';

export type EnterprisesAction = ActionType<typeof actions>;

export interface EnterprisesState {
  readonly loadingEnterprisesRequest: boolean;
  readonly loadingEnterprisesSuccess: boolean;
  readonly error: boolean;
  readonly access_token: string | null;
  readonly client: string | null;
  readonly uid: string | null;
  readonly errMessage: string | null;
  readonly enterprise_name: string | null;
  readonly enterprises: Enterprise[];
}

export interface Enterprise {
  city: string;
  country: string;
  description: string;
  email_enterprise: string;
  enterprise_name: string;
  enterprise_type: {
    enterprise_type_name: string;
    id: number;
  };
  facebook: string;
  id: number;
  linkedin: string;
  own_enterprise: boolean;
  phone: string;
  photo: string;
  share_price: number;
  twitter: string;
  value: number;
}

import { AuthAction, AuthState } from './types';

const INTIAL_STATE: AuthState = {
  loadingSignInRequest: false,
  error: false,
  access_token: null,
  client: null,
  uid: null,
  errMessage: null,
};

export default function auth(
  state = INTIAL_STATE,
  action: AuthAction,
): AuthState {
  switch (action.type) {
    case '@auth/SIGN_IN_REQUEST':
      return {
        ...state,
        loadingSignInRequest: true,
      };

    case '@auth/SIGN_IN_SUCCESS':
      return {
        ...state,
        loadingSignInRequest: false,
        access_token: action.payload.access_token,
        uid: action.payload.uid,
        client: action.payload.client,
        errMessage: null,
      };

    case '@auth/SIGN_IN_RESET':
      return {
        ...state,
        loadingSignInRequest: false,
        errMessage: null,
      };

    case '@auth/SIGN_IN_FAILURE':
      return {
        ...state,
        loadingSignInRequest: false,
        error: true,
        errMessage: action.payload.message,
      };

    default:
      return state;
  }
}

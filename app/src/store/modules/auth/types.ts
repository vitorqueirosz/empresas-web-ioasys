import { ActionType } from 'typesafe-actions';

import * as actions from './actions';

export type AuthAction = ActionType<typeof actions>;

export interface AuthState {
  readonly loadingSignInRequest: boolean;
  readonly error: boolean;
  readonly access_token: string | null;
  readonly client: string | null;
  readonly uid: string | null;
  readonly errMessage: string | null;
}

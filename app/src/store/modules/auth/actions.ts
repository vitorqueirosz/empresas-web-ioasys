import { action } from 'typesafe-actions';

export function signInRequest({
  email,
  password,
}: {
  email: string;
  password: string;
}) {
  return action('@auth/SIGN_IN_REQUEST', { email, password });
}

export function signInSuccess({ access_token, client, uid }: {
  access_token: string; client: string; uid: string}) {
  return action('@auth/SIGN_IN_SUCCESS', {
    access_token,
    client,
    uid,
  });
}

export function signInErrorReset() {
  return action('@auth/SIGN_IN_RESET');
}

export function signInFailure({ message }: { message: string }) {
  return action('@auth/SIGN_IN_FAILURE', { message });
}

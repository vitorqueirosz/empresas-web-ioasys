import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

export function* signIn({ payload }: ActionType<typeof actions.signInRequest>) {
  try {
    const { email, password } = payload;

    const { headers } = yield call(api.post, 'users/auth/sign_in', { email, password });

    const { client, uid } = headers;

    const access_token = headers['access-token'];

    yield put(actions.signInSuccess({ access_token, client, uid }));
  } catch (err) {
    if (err.response.status === 401) {
      yield put(actions.signInFailure({ message: 'Credencias informadas são inválidas, tente novamente' }));
    } else {
      yield put(actions.signInFailure({ message: '' }));
    }
  }
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);

import { Enterprise, EnterpriseAction, EnterpriseState } from './types';

const INTIAL_STATE: EnterpriseState = {
  loadingEnterprisesRequest: false,
  loadingEnterprisesSuccess: false,
  error: false,
  access_token: null,
  client: null,
  uid: null,
  id: null,
  enterprise: {} as Enterprise,
};

export default function auth(
  state = INTIAL_STATE,
  action: EnterpriseAction,
): EnterpriseState {
  switch (action.type) {
    case '@enterprise/GET_ENTERPRISE_REQUEST':
      return {
        ...state,
        loadingEnterprisesRequest: true,
      };

    case '@enterprise/GET_ENTERPRISE_SUCCESS':
      return {
        ...state,
        loadingEnterprisesRequest: false,
        enterprise: action.payload.enterprise,
        loadingEnterprisesSuccess: true,
      };

    case '@enterprise/GET_ENTERPRISE_RESET':
      return {
        ...state,
        loadingEnterprisesRequest: false,
        enterprise: {} as Enterprise,
      };

    case '@enterprise/GET_ENTERPRISE_FAILURE':
      return {
        ...state,
        loadingEnterprisesRequest: false,
        error: true,
      };

    default:
      return state;
  }
}

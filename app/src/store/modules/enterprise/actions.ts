import { action } from 'typesafe-actions';
import { Enterprise } from './types';

export function getEnterpriseRequest({
  access_token,
  client,
  uid,
  id,
}: {
  access_token: string;
  client: string;
  uid: string;
  id: number;
}) {
  return action('@enterprise/GET_ENTERPRISE_REQUEST', {
    access_token,
    client,
    uid,
    id,
  });
}

export function getEnterpriseSuccess({ enterprise }: {
  enterprise: Enterprise}) {
  return action('@enterprise/GET_ENTERPRISE_SUCCESS', {
    enterprise,
  });
}

export function getEnterpriseReset() {
  return action('@enterprise/GET_ENTERPRISE_RESET');
}

export function getEnterpriseFailure() {
  return action('@enterprise/GET_ENTERPRISE_FAILURE');
}

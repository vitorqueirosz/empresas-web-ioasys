import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import api from '../../../services/api';

import * as actions from './actions';

export function* fetchEnterprise({ payload }: ActionType<typeof actions.getEnterpriseRequest>) {
  try {
    const { client, uid, id } = payload;

    const { data } = yield call(api.get, `enterprises/${id}`, {
      headers: {
        'access-token': payload.access_token,
        client,
        uid,
      },
    });

    yield put(actions.getEnterpriseSuccess({ enterprise: data.enterprise }));
  } catch (err) {
    yield put(actions.getEnterpriseFailure());
  }
}

export default all([takeLatest('@enterprise/GET_ENTERPRISE_REQUEST', fetchEnterprise)]);

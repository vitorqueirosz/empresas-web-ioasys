import { combineReducers } from 'redux';

import auth from './auth/reducer';
import enterprises from './enterprises/reducer';
import enterprise from './enterprise/reducer';

const rootReducer = combineReducers({
  auth,
  _persist: (state: any = null) => state,
  enterprises,
  enterprise,
});

export type ApplicationState = ReturnType<typeof rootReducer>;

export { rootReducer };

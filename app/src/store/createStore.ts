import {
  applyMiddleware,
  createStore,
  Middleware,
  Reducer,
  Store,
} from 'redux';
import { PersistState } from 'redux-persist';
import { AuthAction, AuthState } from './modules/auth/types';
import { EnterpriseAction, EnterpriseState } from './modules/enterprise/types';
import { EnterprisesAction, EnterprisesState } from './modules/enterprises/types';

export interface StoreState {
  _persist: PersistState;
  auth: AuthState;
  enterprises: EnterprisesState;
  enterprise: EnterpriseState;
}

export type StoreAction = AuthAction | EnterprisesAction | EnterpriseAction;

export default (
  reducers: Reducer<StoreState, StoreAction>,
  middlewares: Middleware[],
): Store => {
  const enhancer = applyMiddleware(...middlewares);

  return createStore(reducers, enhancer);
};

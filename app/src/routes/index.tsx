import React from 'react';
import { Switch, BrowserRouter } from 'react-router-dom';
import Home from '../pages/Home';

import Route from './Route';

import SignIn from '../pages/SignIn';
import Search from '../pages/Search';
import Details from '../pages/Details';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={SignIn} />
        <Route path="/home" isPrivate component={Home} />
        <Route path="/search" isPrivate component={Search} />
        <Route path="/details" isPrivate component={Details} />
      </Switch>
    </BrowserRouter>
  );
}

import React from 'react';
import { useSelector } from 'react-redux';

import {
  RouteProps as ReactDOMRouteProps,
  Route as ReactDOMRoute,
  Redirect,
} from 'react-router-dom';
import { StoreState } from '../store/createStore';

interface RouteProps extends ReactDOMRouteProps {
    isPrivate?: boolean;
    component: React.ComponentType;
}

const Route: React.FC<RouteProps> = ({
  isPrivate = false,
  component: Component,
  ...rest
}) => {
  const { access_token, client, uid } = useSelector((state: StoreState) => state.auth);

  return (
    <ReactDOMRoute
      {...rest}
      render={
          ({ location }) => (isPrivate === (!!access_token && !!client && !!uid) ? (
            <Component />
          ) : (
            <Redirect to={{
              pathname: isPrivate ? '/' : 'home',
              state: { from: location },
            }}
            />
          )
          )
      }
    />
  );
};

export default Route;

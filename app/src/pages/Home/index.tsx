import Header from '../../components/Header';

import { Container } from './styles';

const Home = (): JSX.Element => (
  <Container>
    <Header />

    <div>
      <span>Clique na busca para iniciar.</span>
    </div>
  </Container>
);

export default Home;

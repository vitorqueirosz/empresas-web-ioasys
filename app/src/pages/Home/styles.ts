import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  width: 100vw;
  flex-direction: column;


  > div {

    display: flex;
    justify-content: center;
    align-items: center;
    margin: auto;

    > span {
      color: #383743;
      font-size: 2rem;
    }
  }
`;

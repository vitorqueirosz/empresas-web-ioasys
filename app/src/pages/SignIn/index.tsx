import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FaLock } from 'react-icons/fa';
import { FiMail } from 'react-icons/fi';

import * as Yup from 'yup';

import { useFormik } from 'formik';

import logoIoasys from '../../assets/logo-home.png';
import Input from '../../components/Input';
import Button from '../../components/Button';

import { signInErrorReset, signInRequest } from '../../store/modules/auth/actions';
import { StoreState } from '../../store/createStore';

import {
  Container,
  InputContainer,
  ButtonContainer,
  Form,
  ErrorMessageContainer,
} from './styles';
import Loading from './Loading';

interface SignInData {
  email: string;
  password: string
}

const SignIn = () => {
  const dispatch = useDispatch();

  const schemaValidate = Yup.object().shape({
    email: Yup.string()
      .required('E-mail obrigatório')
      .email('Digite um e-mail válido'),
    password: Yup.string()
      .required('Senha não pode ficar vazia')
      .min(8, () => 'Sua senha está muito pequena'),
  });

  const {
    handleSubmit,
    handleChange,
    errors,
    values,
    touched,
    handleBlur,
  } = useFormik({
    initialValues: { email: '', password: '' },
    onSubmit: (data: SignInData) => handleLogin(data),
    validationSchema: schemaValidate,
  });

  useEffect(() => {
    dispatch(signInErrorReset());
  }, [dispatch]);

  const handleLogin = useCallback((data: any) => {
    const { email, password } = data;

    dispatch(signInRequest({ email, password }));
  }, [dispatch]);

  const { errMessage, loadingSignInRequest } = useSelector((state: StoreState) => state.auth);

  return (

    <Container>
      <img src={logoIoasys} alt="logo" />

      <div>
        <h1>BEM-VINDO AO EMPRESAS</h1>

        <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
      </div>

      <Form onSubmit={handleSubmit}>
        <InputContainer>
          <Input
            isError={!!errors.email && !!touched.email}
            focused={!touched.email}
            icon={FiMail}
            type="email"
            placeholder="E-mail"
            onChange={handleChange('email')}
            error={errors.email}
            isFilled={!!values.email}
            onBlur={handleBlur('email')}
            submitError={!!errMessage}
          />

          <Input
            isError={!!errors.password && !!touched.password}
            icon={FaLock}
            isPassword
            type="password"
            placeholder="Senha"
            focused={!touched.password}
            isFilled={!!values.password}
            onChange={handleChange('password')}
            onBlur={handleBlur('password')}
            error={errors.password}
            submitError={!!errMessage}
          />
        </InputContainer>

        {errMessage && (
          <ErrorMessageContainer>
            <span>{errMessage}</span>
          </ErrorMessageContainer>
        )}

        <ButtonContainer>
          <Button type="submit">ENTRAR</Button>
        </ButtonContainer>
      </Form>

      {loadingSignInRequest && (
        <Loading isLoading={loadingSignInRequest} />
      )}
    </Container>

  );
};

export default SignIn;

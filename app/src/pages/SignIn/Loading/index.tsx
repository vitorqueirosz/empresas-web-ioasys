import React from 'react';
import { FaSpinner } from 'react-icons/fa';

import { Container, SpinnerContainer } from './styles';

interface LoadingProps {
  isLoading: boolean;
}

const Loading = ({ isLoading }: LoadingProps): JSX.Element => (
  <Container>
    <SpinnerContainer isLoading={isLoading}>
      <FaSpinner size={25} color="#57bbbc" />
    </SpinnerContainer>
  </Container>
);

export default Loading;

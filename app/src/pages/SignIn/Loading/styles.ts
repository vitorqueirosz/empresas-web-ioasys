import styled, { css, keyframes } from 'styled-components';

interface LoadingContainerProps {
  isLoading?: boolean;
}

const rotate = keyframes`
    from {
        transform: rotate(0deg);
    }to {
        transform: rotate(360deg);
    }
`;

export const Container = styled.div<LoadingContainerProps>`

  height: 100vh;
  width: 100vw;
  background: rgba(255, 255, 255, 0.6);
  position: absolute;
`;

export const SpinnerContainer = styled.div<LoadingContainerProps>`

  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;


    ${(props) => props.isLoading
      && css`
      svg {
          animation: ${rotate} 2s linear infinite;
      }
  `}

`;

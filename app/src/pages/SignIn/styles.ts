import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  position: relative;

  flex-direction: column;





 > div {


    flex-direction: column;
    margin-top: 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0 64px;

    h1 {
      text-align: center;
      max-width: 150px;
      line-height: 24px;
      color: #383743;
      font-size: 1.8rem;
      margin-top: 4.188rem;
    }

      p {

      margin-top: 1.281rem;
      max-width: 200px;
      font-size: 1.125rem;
      text-align: center;
      color: #383743
    }


    @media (max-width: 700px) {
      h1 {
          line-height: 36px;
        }
      }
  }
`;

export const InputContainer = styled.div`
  margin-top: 3.55rem;
`;

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Form = styled.form`
  width: 100%;
  max-width: 300px;

    @media (max-width: 700px) {

      > div {
        width: 100%;
        max-width: 300px;
      }
    }
`;

export const ErrorMessageContainer = styled.div`
  justify-content: center;
  align-items: center;
  display: flex;
  margin-top: 4px;

  > span {
    color: #ff0f44;
    margin: 2px 0;
    font-size: 11px;
  }
`;

import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  width: 100vw;

  @media (max-width: 700px) {
    div {

      padding: 0 32px;

      div {
        flex-direction: column;
        justify-content: center;

        img {
          width: 18.5rem;
          height: 10.438rem;
        }

        span {
          color: #8d8c8c;
          font-size: 1.5rem;
          margin-top: 2rem;
          margin-left: 0;
        }
      }
    }
  }


  header {
    background: linear-gradient(173deg, #ee4c77 24%,  #ee4c77  66%);
    height: 93px;
    width: 100vw;
    padding: 0 32px;
    display: flex;
    align-items: center;

    a {
      border: 0;
      background: none;
      text-decoration: none;
      display: flex;
      align-items: center;
    }

    span {
      font-size: 2.125rem;
      color: #fff;
      margin-left: 3rem;
    }
  }
`;

export const EnterpriseItem = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 4.375rem;
  padding: 0 64px;

  > div {
    display: flex;
    align-items: center;
    width: 100%;
    background: #fff;
    padding: 24px;

    img {
      width: 48.5rem;
      height: 18.438rem;
    }

    span {
      color: #8d8c8c;
      font-size: 1.5rem;
      margin-left: 5rem;
    }
  }
`;

import React, { useCallback, useEffect } from 'react';
import { FiArrowLeft } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { StoreState } from '../../store/createStore';
import { getEnterpriseRequest, getEnterpriseReset } from '../../store/modules/enterprise/actions';
import { Enterprise } from '../../store/modules/enterprise/types';

import { Container, EnterpriseItem } from './styles';

const Details = (): JSX.Element => {
  const { state } = useLocation();

  const detail = state as Enterprise;

  const dispatch = useDispatch();

  const { access_token, client, uid } = useSelector((store: StoreState) => store.auth);

  const { enterprise } = useSelector((store: StoreState) => store.enterprise);

  const handleResetEnterpriseRequest = useCallback(() => {
    if (access_token && client && uid) {
      dispatch(getEnterpriseRequest({
        access_token, client, uid, id: detail.id,
      }));
    }
  }, [dispatch, access_token, client, uid, detail.id]);

  useEffect(() => {
    handleResetEnterpriseRequest();
  }, [handleResetEnterpriseRequest]);

  useEffect((): any => () => dispatch(getEnterpriseReset()), [dispatch]);

  return (

    <Container>
      <header>
        <Link to="/search">
          <FiArrowLeft size={25} color="#fff" />
          <span>{enterprise.enterprise_name}</span>
        </Link>
      </header>

      <EnterpriseItem>
        <div>
          <img src={`https://empresas.ioasys.com.br/${enterprise.photo}`} alt="" />
          <span>{enterprise.description}</span>
        </div>
      </EnterpriseItem>
    </Container>
  );
};

export default Details;

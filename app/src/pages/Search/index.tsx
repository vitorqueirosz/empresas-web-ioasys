import { useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import Header from '../../components/Header';

import { StoreState } from '../../store/createStore';

import { Container, FetchedEnterprises, EnterpriseItem } from './styles';

const Search = (): JSX.Element => {
  const { enterprises, loadingEnterprisesSuccess } = useSelector(
    (state: StoreState) => state.enterprises,
  );

  return (
    <Container>
      <Header isSearch />

      <div>
        {!enterprises.length && loadingEnterprisesSuccess ? (

          <div>
            <span>Nenhuma empresa foi encontrada para a busca realizada.</span>
          </div>

        ) : (
          <FetchedEnterprises>
            {enterprises.map((enterprise) => (
              <EnterpriseItem key={enterprise.id}>
                <img src={`https://empresas.ioasys.com.br/${enterprise.photo}`} alt="enterprisePhoto" />

                <div>
                  <Link to={{
                    pathname: '/details',
                    state: enterprise,
                  }}
                  >
                    <h1>{enterprise.enterprise_name}</h1>
                  </Link>
                  <span>{enterprise.enterprise_type.enterprise_type_name}</span>
                  <small>{enterprise.country}</small>
                </div>
              </EnterpriseItem>
            ))}
          </FetchedEnterprises>
        )}
      </div>
    </Container>
  );
};

export default Search;

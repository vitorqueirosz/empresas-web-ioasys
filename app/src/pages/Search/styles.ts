import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  width: 100vw;
  flex-direction: column;

  @media (max-width: 700px) {

    > div {
      padding: 0 48px;
    }
  }

  > div {
    display: flex;
    height: 50%;
    width: 100%;
    margin-top: 2.75rem;
    flex-direction: column;
    align-items: center;

    > div {
      display: flex;
      justify-content: center;
      align-items: center;
      margin: auto;

      span {
        font-size: 2.125rem;
        color: #b5b4b4;
      }
    }
  }
`;

export const FetchedEnterprises = styled.ul`
  flex-direction: column;
  display: flex;
  justify-content: flex-start;
  align-items: center;

  @media (max-width: 700px) {

    li {
      flex-direction: column;
      height: 20rem;
      width: 22.69rem;

      div {
        margin-top: 2rem;
        margin-left: 0;
      }
    }
  }
`;

export const EnterpriseItem = styled.li`

  display: flex;
  align-items: center;
  background: #fff;
  width: 57.69rem;
  height: 13.366rem;
  padding: 16px;
  margin-bottom: 24px;


  > img {
    width: 18.313rem;
    height: 10rem;
  }

  > div {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin-left: 2.461rem;

    a {
      /* border: 0;
      background: transparent; */
      text-decoration: none;

      h1 {
        color: #1a0e49;
        font-size: 1.875rem;
      }
    }



    span {
      color: #8d8c8c;
      font-size: 1.5rem;
      margin-top: 2px;
    }

    small {
      color: #8d8c8c;
      font-size: 1.125rem;
      margin-top: 4px;
    }
  }
`;
